﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.Windows.Forms;
using System.IO;
namespace CamProto
{
    class ExcelControl
    {
        string fileLocation;

        public ExcelControl(string filePath)
        {
            fileLocation = filePath;
        }

        int GetLastUsedRow(ExcelWorksheet sheet)
        {
            if (sheet.Dimension == null) { return 0; } // In case of a blank sheet
            var row = sheet.Dimension.End.Row;
            while (row >= 1)
            {
                var range = sheet.Cells[row, 1, row, sheet.Dimension.End.Column];
                if (range.Any(c => !string.IsNullOrWhiteSpace(c.Text)))
                {
                    break;
                }
                row--;
            }
            return row;
        }

        public bool saveValuesToExcel(List<string> values)
        {

            FileInfo fi = new FileInfo(fileLocation);
            ExcelPackage exPackage = new ExcelPackage(fi);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelWorksheet wrkShtData = exPackage.Workbook.Worksheets[0];
            if (values.Count > 0)
            {
                try
                {
                    var lastRow = GetLastUsedRow(wrkShtData) + 1;
                    for (int i = 1; i < 45; i++)
                    {
                        wrkShtData.Cells[lastRow, i].Value = values[i-1];
                    }
                    exPackage.Save();
                    return true;
                }
                catch (Exception e)
                {
                    MessageBox.Show("Cannot save excel values in control method - try closing Excel: " + e.Message);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Cannot save excel values - they are empty.");
                return false;
            }
        }

    }
}
