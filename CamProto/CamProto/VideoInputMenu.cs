﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using WMPLib;
namespace CamProto
{
    public partial class VideoInputMenu : Form
    {
        NavigateVideos allVideos;
        public VideoInputMenu()
        {
            InitializeComponent();
            allVideos = new NavigateVideos(MainMenu.mainDirectory + "\\unentered");
            //Reset all fields except intials
            resetNonBearControls();
            setBearControls(false);
            txtBearCount.Text = "0";

            playFile(allVideos.getCurrentVideo());

        }

        void playFile(string videoURL)
        {
            string currDateTime = "";
            if (allVideos.getCurrentVideo() != "-1")
            {
                wmpMainPlayer.URL = videoURL;
                timerVideo.Tick += new EventHandler(timerVideo_Tick);
                timerVideo.Enabled = true;
                lblCurrDirTxt.Text = videoURL;
                txtRevisitDate.Text = allVideos.currRevisitDate();
                txtSiteNumber.Text = allVideos.currSiteNumber();
                txtSiteName.Text = allVideos.currSiteName();

                txtSiteID.Text = allVideos.currSiteID();
                txtFileName.Text = allVideos.currFileName();
                wmpMainPlayer.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(wmp_PlayStateChange);
                txtVideoDuration.Text = "";
                try
                {
                    currDateTime = Convert.ToDateTime(allVideos.currVideoDateAndTime()).ToString("dd/MM/yyyy HH:mm");
                    currDateTime = currDateTime.Replace("-", "/");
                    txtVideoDate.Text = currDateTime.Substring(0, 10);
                    txtTimeAtVideoStart.Text = currDateTime.Substring(11, 5);
                }
                catch (Exception eee)
                {
                    Console.WriteLine(eee.Message);
                }
                finally { }
            }
            else
            {
                MessageBox.Show("No videos found! Have you moved them while this is running?");
            }


        }

        void wmp_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 3 && txtVideoDuration.Text == "")
            {
                if (wmpMainPlayer.currentMedia.durationString.Length <= 5)
                {
                    txtVideoDuration.Text = "00:" + wmpMainPlayer.currentMedia.durationString;
                }
                else
                {
                    txtVideoDuration.Text = wmpMainPlayer.currentMedia.durationString;
                }
            }
        }

        private void timerVideo_Tick(object Sender, EventArgs e)
        {
            // Set the caption to the current time.  
            if ((int)(Math.Truncate((wmpMainPlayer.Ctlcontrols.currentPosition / wmpMainPlayer.currentMedia.duration) * 100)) >= 0)
                trackBarVideoTracking.Value = (int)(Math.Truncate((wmpMainPlayer.Ctlcontrols.currentPosition / wmpMainPlayer.currentMedia.duration) * 100));
        }
        private void txtHumanCount_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxHumansDrifting_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxDriftBoats_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxHumansInNonDriftBoat_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxNonDriftBoats_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxHumansFishingFromShore_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtTotalHumansOnPlatform_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtTotalOtherHumans_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void listBoxOtherAnimal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxOtherAnimal.SelectedIndex == 0)
            {
                txtOtherAnimSppName.Enabled = true;
            }
            else if (listBoxOtherAnimal.SelectedIndex == 1)
            {
                txtOtherAnimSppName.Enabled = false;
            }
        }

        //bear count
        private void txtBearCount_TextChanged(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        void setBearControls(bool isActive)
        {
            listBoxBearSpecies.Enabled = isActive;
            listBoxBearSpecies.SelectedIndex = 0;
            txtCOYCount.Text = "N/A";
            //sexing
            listBoxSex1.Enabled = isActive;
            listBoxSex1.SelectedIndex = 0;
            listBoxSex2.Enabled = isActive;
            listBoxSex2.SelectedIndex = 0;
            listBoxSex3.Enabled = isActive;
            listBoxSex3.SelectedIndex = 0;
            listBoxSex4.Enabled = isActive;
            listBoxSex4.SelectedIndex = 0;

            //age
            listBoxBearAge1.Enabled = isActive;
            listBoxBearAge1.SelectedIndex = 0;
            listBoxBearAge2.Enabled = isActive;
            listBoxBearAge2.SelectedIndex = 0;
            listBoxBearAge3.Enabled = isActive;
            listBoxBearAge3.SelectedIndex = 0;
            listBoxBearAge4.Enabled = isActive;
            listBoxBearAge4.SelectedIndex = 0;

            //family groups
            listBoxFamilyGroup.Enabled = isActive;
            listBoxFamilyGroup.SelectedIndex = 0;
            txtCOYCount.Enabled = isActive;
            txtCOYCount.Text = "N/A";
            txtBoxYearlingCount.Enabled = isActive;
            txtBoxYearlingCount.Text = "N/A";

            //other
            listBoxBearFishInteraction.Enabled = isActive;
            listBoxBearFishInteraction.SelectedIndex = 0;
            listBoxSnagAvoidance.Enabled = isActive;
            listBoxSnagAvoidance.SelectedIndex = 0;
            txtSnagAvoidanceComments.Enabled = isActive;
            txtSnagAvoidanceComments.Text = "";
            listBoxCamAwareness.Enabled = isActive;
            listBoxCamAwareness.SelectedIndex = 0;
            txtCamAwarenessComm.Enabled = isActive;
            txtCamAwarenessComm.Text = "";


        }

        private void txtBearCount_LostFocus(object sender, System.EventArgs e)
        {
            int bearCount;
            //check numeric value
            if (!int.TryParse(((TextBox)sender).Text, out bearCount))
            {
                ((TextBox)sender).Text = "0";
                setBearControls(false);
                MessageBox.Show("Must be a numeric, whole number and positive value.", "Input error");
            }
            else
            {
                //if less than 0
                if (bearCount < 0)
                {
                    ((TextBox)sender).Text = "0";
                    setBearControls(false);
                    MessageBox.Show("Value must be positive.", "Input error");
                } else if (bearCount == 0)
                {
                    //disable all bear controls
                    setBearControls(false);
                } else if (bearCount > 0)
                {
                    //enable all bear controls
                    setBearControls(true);
                }
            }

        }

        private void txtCOYCount_LostFocus(object sender, System.EventArgs e)
        {
            int number;
            int bearCount = 0;
            int.TryParse(txtBearCount.Text, out bearCount);

            //if it is n/a then leave it
            if (((TextBox)sender).Text == "N/A")
            {
                return;
            }


            if (!int.TryParse(((TextBox)sender).Text, out number) || number < 0 || number == 0 || number > bearCount)
            {
                ((TextBox)sender).Text = "N/A";
                MessageBox.Show("Count must be N/A or a positive >0 number and cannot be more than the bear count.");
            } 
        }

        void resetNonBearControls()
        {
            //meta
            txtSiteName.Text = "";
            txtSiteNumber.Text = "";
            txtRevisitDate.Text = "";
            txtSiteID.Text = "";
            txtFileName.Text = "";
            txtVideoDate.Text = "";
            txtVideoDuration.Text = "";
            txtTimeAtVideoStart.Text = "";
            //human
            txtHumanCount.Text = "0";
            listBoxCamHandle.SelectedIndex = 0;
            txtMaxDriftBoats.Text = "0";
            txtMaxHumansDrifting.Text = "0";
            txtMaxHumansInNonDriftBoat.Text = "0";
            txtMaxNonDriftBoats.Text = "0";
            txtMaxHumansFishingFromShore.Text = "0";
            txtTotalHumansOnPlatform.Text = "0";
            txtTotalOtherHumans.Text = "0";
            txtHumanComments.Text = "";
            //Other animals
            listBoxOtherAnimal.SelectedIndex = 0;
            txtOtherAnimSppName.Text = "";
            //general comments and verification
            txtGeneralComments.Text = "";
            txtVerification.Text = "";
            txtTargetIssues.Text = "";
        }


        string getSexDropValue(object sender)
        {
            if (((ComboBox)sender).SelectedItem is null || ((ComboBox)sender).SelectedIndex == 0)
            {
                return "N/A";
            }
            else if (((ComboBox)sender).SelectedIndex == 1)
            {
                return "U";
            }
            else if (((ComboBox)sender).SelectedIndex == 2)
            {
                return "M";
            }else if (((ComboBox)sender).SelectedIndex == 3)
            {
                return "F";
            }

            return "U";

        }

        string getAgeDropValue(object sender)
        {
            if (((ComboBox)sender).SelectedItem is null || ((ComboBox)sender).SelectedIndex == 0)
            {
                return "N/A";
            }
            else if (((ComboBox)sender).SelectedIndex == 1)
            {
                return "U";
            }
            else if (((ComboBox)sender).SelectedIndex == 2)
            {
                return "ADULT";
            }
            else if (((ComboBox)sender).SelectedIndex == 3)
            {
                return "SUBADULT";
            }

            return "U";
        }


        bool checkDirectories()
        {
            bool valid = true;
            //check if folders exist

            //ID and Name
            //Revisit Date
            //unentered - TopDirectory/entered
            try
            {
                // Try to create the directory.
                Directory.CreateDirectory(MainMenu.mainDirectory + "\\entered");
            }
            catch (Exception exc)
            {
                MessageBox.Show("Could not create entered directory, do you have write permission? Error: " + exc.Message);
                valid = false;
            }
            finally { }

            return valid;
        }

        List<string> excelValues()
        {
            List<string> allVals = new List<string>();
            try
            {
                //initials
                allVals.Add(txtInitials.Text);
                allVals.Add(txtSiteName.Text);
                allVals.Add(txtSiteNumber.Text);
                allVals.Add(txtSiteID.Text);
                allVals.Add(txtRevisitDate.Text);
                allVals.Add(txtFileName.Text);
                allVals.Add(txtVideoDate.Text);
                allVals.Add(txtTimeAtVideoStart.Text);
                allVals.Add(txtVideoDuration.Text);
                allVals.Add(txtHumanCount.Text);

                if (listBoxCamHandle.SelectedItem is null || listBoxCamHandle.SelectedIndex == 0)
                {
                    allVals.Add("N");
                    allVals.Add("N");
                    allVals.Add("N");
                }
                else if (listBoxCamHandle.SelectedIndex == 1)
                {
                    allVals.Add("Y");
                    allVals.Add("N");
                    allVals.Add("N");
                }
                else if (listBoxCamHandle.SelectedIndex == 2)
                {
                    allVals.Add("N");
                    allVals.Add("Y");
                    allVals.Add("N");
                }
                else if (listBoxCamHandle.SelectedIndex == 3)
                {
                    allVals.Add("N");
                    allVals.Add("N");
                    allVals.Add("Y");
                }

                allVals.Add(txtMaxHumansDrifting.Text);
                allVals.Add(txtMaxDriftBoats.Text);
                allVals.Add(txtMaxHumansInNonDriftBoat.Text);
                allVals.Add(txtMaxNonDriftBoats.Text);
                allVals.Add(txtMaxHumansFishingFromShore.Text);
                allVals.Add(txtTotalHumansOnPlatform.Text);
                allVals.Add(txtTotalOtherHumans.Text);
                if (txtHumanComments.Text.Replace(" ", "") == "")
                {
                    allVals.Add("N/A");
                }
                else
                {
                    allVals.Add(txtHumanComments.Text);
                }

                if (listBoxOtherAnimal.SelectedItem is null || listBoxOtherAnimal.SelectedIndex == 0)
                {
                    allVals.Add("N");
                    txtOtherAnimSppName.Text = "N/A";
                }
                else if (listBoxOtherAnimal.SelectedIndex == 1)
                {
                    allVals.Add("Y");

                }


                allVals.Add(txtOtherAnimSppName.Text);
                allVals.Add(txtBearCount.Text);

                int count = 0;
                bool parseBearCount = int.TryParse(txtBearCount.Text, out count);

                if (parseBearCount == true && count > 0)
                {

                    if (listBoxBearSpecies.SelectedItem is null || listBoxBearSpecies.SelectedIndex == 0)
                    {
                        allVals.Add("B");
                    }
                    else if (listBoxBearSpecies.SelectedIndex == 1)
                    {
                        allVals.Add("G");

                    }

                    allVals.Add(getSexDropValue(listBoxSex1));
                    allVals.Add(getSexDropValue(listBoxSex2));
                    allVals.Add(getSexDropValue(listBoxSex3));
                    allVals.Add(getSexDropValue(listBoxSex4));
                    allVals.Add(getAgeDropValue(listBoxBearAge1));
                    allVals.Add(getAgeDropValue(listBoxBearAge2));
                    allVals.Add(getAgeDropValue(listBoxBearAge3));
                    allVals.Add(getAgeDropValue(listBoxBearAge4));

                    if (listBoxFamilyGroup.SelectedItem is null || listBoxFamilyGroup.SelectedIndex == 0)
                    {
                        allVals.Add("N/A");
                    }
                    else if (listBoxFamilyGroup.SelectedIndex == 1)
                    {
                        allVals.Add("N");
                    }
                    else if (listBoxFamilyGroup.SelectedIndex == 2)
                    {
                        allVals.Add("Y");
                    }

                    allVals.Add(txtCOYCount.Text);
                    allVals.Add(txtBoxYearlingCount.Text);

                    if (listBoxBearFishInteraction.SelectedItem is null || listBoxBearFishInteraction.SelectedIndex == 0)
                    {
                        allVals.Add("N/A");
                    }
                    else if (listBoxBearFishInteraction.SelectedIndex == 1)
                    {
                        allVals.Add("N");
                    }
                    else if (listBoxBearFishInteraction.SelectedIndex == 2)
                    {
                        allVals.Add("Y");
                    }


                    if (listBoxSnagAvoidance.SelectedItem is null || listBoxSnagAvoidance.SelectedIndex == 0)
                    {
                        allVals.Add("N/A");
                        allVals.Add("N/A");
                    }
                    else if (listBoxSnagAvoidance.SelectedIndex == 1)
                    {
                        allVals.Add("N");
                        allVals.Add(txtSnagAvoidanceComments.Text);
                    }
                    else if (listBoxSnagAvoidance.SelectedIndex == 2)
                    {
                        allVals.Add("Y");
                        allVals.Add(txtSnagAvoidanceComments.Text);
                    }

                    if (listBoxCamAwareness.SelectedItem is null || listBoxCamAwareness.SelectedIndex == 0)
                    {
                        allVals.Add("N/A");
                        allVals.Add("N/A");
                    }
                    else if (listBoxCamAwareness.SelectedIndex == 1)
                    {
                        allVals.Add("N");
                        allVals.Add(txtCamAwarenessComm.Text);
                    }
                    else if (listBoxCamAwareness.SelectedIndex == 2)
                    {
                        allVals.Add("Y");
                        allVals.Add(txtCamAwarenessComm.Text);
                    }
                }
                else
                {
                    allVals.Add("N/A"); //bear speecies
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                    allVals.Add("N/A");
                }
                ///


                if (txtGeneralComments.Text.Replace(" ", "") == "")
                {
                    allVals.Add("N/A");
                }
                else
                {
                    allVals.Add(txtGeneralComments.Text);
                }

                if (txtVerification.Text.Replace(" ", "") == "")
                {
                    allVals.Add("N/A");
                }
                else
                {
                    allVals.Add(txtVerification.Text);
                }

                if (txtTargetIssues.Text.Replace(" ", "") == "")
                {
                    allVals.Add("N/A");
                }
                else
                {
                    allVals.Add(txtTargetIssues.Text);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not save excel values: " + e.Message);
            }
            return allVals;
        }

        private void btnEnterAndNextVideo_Click(object sender, EventArgs e)
        {
            bool excelSucc = false;
            bool moveSucc = false;

            string validationMessage;
            //validate fields
            if (checkDirectories() == true)
            {
                if (allVideos.getCurrentVideo() != "-1")
                {
                    if (validation(out validationMessage) == true)
                    {
                        //convert all fields to excel values
                        //do all the above
                        ExcelControl ex = new ExcelControl(MainMenu.excelFile);
                        excelSucc = ex.saveValuesToExcel(excelValues());

                        //Check if have access to video rename, moving, excel adding and saving
                        //create folders
                        new FileInfo(MainMenu.mainDirectory + "\\entered\\" + allVideos.currSiteNumber() + " " + allVideos.currSiteName() + "\\" + "Revisit Date - " + allVideos.currRevisitDate().Replace("/", "-") + "\\").Directory.Create();
                        //move and rename file
                        moveSucc = allVideos.moveCurrentVideo(MainMenu.mainDirectory + "\\entered\\" + allVideos.currSiteNumber() + " " + allVideos.currSiteName() + "\\" + "Revisit Date - " + allVideos.currRevisitDate().Replace("/", "-") + "\\" + txtFileName.Text);

                        //Reset all fields except intials
                        resetNonBearControls();
                        setBearControls(false);
                        txtBearCount.Text = "0";
                        //Get camera image text data

                        if(excelSucc == true && moveSucc == false)
                        {
                            MessageBox.Show("Excel entries saved successfuly, however the file could not be moved.");
                        }else if(excelSucc == false && moveSucc == true)
                        {
                            MessageBox.Show("File move completed, however EXCEL ENTRY COULD --NOT-- BE SAVED! Halting. If Excel is open, close it and restart the program. If you want entry for this video, you need to move it back to unentered.");
                        }

                        if (allVideos.getCurrentVideo() != "-1" && excelSucc == true)
                        {
                            //play next video
                            playFile(allVideos.getCurrentVideo());
                        }
                        else
                        {
                            MessageBox.Show("Can't find any more video files!");
                            lblCurrDirTxt.Text = "";
                            wmpMainPlayer.URL = "";
                        }
                    }
                    else
                    {
                        MessageBox.Show(validationMessage);
                    }
                }
                else
                {
                    MessageBox.Show("Can't find any more video files!");
                    lblCurrDirTxt.Text = "";
                    wmpMainPlayer.URL = "";
                }
            }

        }

        private bool validation(out string validationMessage)
        {
            validationMessage = "";
            string tempString = "";

            //metadata
            tempString = txtInitials.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0)
            {
                validationMessage = "Please enter initials.";
                return false;
            }

            tempString = txtVideoDate.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0 || DateTime.TryParseExact(tempString, "dd/mm/yyyy",
                new CultureInfo("en-US"),
                              DateTimeStyles.None,
                              out _) == false)
            {
                validationMessage = "Invalid video date.";
                return false;
            }

            tempString = txtRevisitDate.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0 || DateTime.TryParseExact(tempString, "dd/mm/yyyy",
                new CultureInfo("en-US"),
                              DateTimeStyles.None,
                              out _) == false)
            {
                validationMessage = "Invalid revisit date.";
                return false;
            }

            tempString = txtSiteName.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0)
            {
                validationMessage = "Enter a site name";
                return false;
            }

            tempString = txtSiteID.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0)
            {
                validationMessage = "Enter a site ID";
                return false;
            }

            tempString = txtFileName.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0)
            {
                validationMessage = "Enter a file name";
                return false;
            }

            tempString = txtSiteNumber.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0)
            {
                validationMessage = "Enter a site number";
                return false;
            }

            tempString = txtVideoDuration.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0 || DateTime.TryParseExact(tempString, "hh:mm:ss",
                new CultureInfo("en-US"),
                              DateTimeStyles.None,
                              out _) == false)
            {
                validationMessage = "Invalid video duration.";
                return false;
            }


            tempString = txtTimeAtVideoStart.Text;
            tempString = Regex.Replace(tempString, @"\s+", "");
            if (tempString.Length == 0 || DateTime.TryParseExact(tempString, "HH:mm",
                new CultureInfo("en-US"),
                              DateTimeStyles.None,
                              out _) == false)
            {
                validationMessage = "Invalid time of video start.";
                return false;
            }

            //human counts
            if (int.TryParse(txtHumanCount.Text, out _) == false || txtHumanCount.Text.Length == 0)
            {
                validationMessage = "Human count must be numeric value.";
                return false;
            }
            if (int.TryParse(txtMaxHumansDrifting.Text, out _) == false || txtMaxHumansDrifting.Text.Length == 0)
            {
                validationMessage = "Max humans drifting must be numeric value.";
                return false;
            }
            if (int.TryParse(txtMaxDriftBoats.Text, out _) == false || txtMaxDriftBoats.Text.Length == 0)
            {
                validationMessage = "Max drift boats must be numeric value.";
                return false;
            }
            if (int.TryParse(txtMaxHumansInNonDriftBoat.Text, out _) == false ||txtMaxHumansInNonDriftBoat.Text.Length == 0)
            {
                validationMessage = "Max humans in non-driftboats must be numeric value.";
                return false;
            }
            if (int.TryParse(txtMaxNonDriftBoats.Text, out _) == false || txtMaxNonDriftBoats.Text.Length == 0)
            {
                validationMessage = "Max non-driftboats must be numeric value.";
                return false;
            }
            if (int.TryParse(txtMaxHumansFishingFromShore.Text, out _) == false || txtMaxHumansFishingFromShore.Text.Length == 0)
            {
                validationMessage = "Max humans fishing from shore must be numeric value.";
                return false;
            }
            if (int.TryParse(txtTotalHumansOnPlatform.Text, out _) == false || txtTotalHumansOnPlatform.Text.Length == 0)
            {
                validationMessage = "Human count must be numeric value.";
                return false;
            }
            if (int.TryParse(txtTotalOtherHumans.Text, out _) == false || txtTotalOtherHumans.Text.Length == 0)
            {
                validationMessage = "Total other humans must be numeric value.";
                return false;
            }

            //other animal
            if(listBoxOtherAnimal.SelectedIndex == 1 && txtOtherAnimSppName.Text == "")
            {
                validationMessage = "If other animal present is yes, please enter species name.";
            }

            //bear counts
            if (int.TryParse(txtBearCount.Text, out _) == false || txtBearCount.Text.Length == 0)
            {
                validationMessage = "Bear count must be numeric value.";
                return false;
            }

            return true;
        }

        private void trackBarVideoTracking_Scroll(object sender, EventArgs e)
        {
            wmpMainPlayer.Ctlcontrols.currentPosition = (wmpMainPlayer.currentMedia.duration * trackBarVideoTracking.Value) / 100;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void VideoInputMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void listBoxCamHandle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBoxOtherAnimal_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (listBoxOtherAnimal.SelectedIndex == 1)
            {
                txtOtherAnimSppName.Enabled = true;
            }
            else if (listBoxOtherAnimal.SelectedIndex == 0)
            {
                txtOtherAnimSppName.Text = "";
                txtOtherAnimSppName.Enabled = false;
            }

        }

        private void txtBearCount_TextChanged(object sender, EventArgs e)
        {
            int bearCount;
            //check numeric value
            if (!int.TryParse(((TextBox)sender).Text, out bearCount))
            {
                ((TextBox)sender).Text = "0";
                setBearControls(false);
                MessageBox.Show("Must be a numeric, whole number and positive value.", "Input error");
            }
            else
            {
                //if less than 0
                if (bearCount < 0)
                {
                    ((TextBox)sender).Text = "0";
                    setBearControls(false);
                    MessageBox.Show("Value must be positive.", "Input error");
                }
                else if (bearCount == 0)
                {
                    //disable all bear controls
                    setBearControls(false);
                }
                else if (bearCount > 0)
                {
                    //enable all bear controls
                    setBearControls(true);
                }
            }
        }


    }


}
