﻿namespace CamProto
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.btnSelOverallDirectory = new System.Windows.Forms.Button();
            this.lblMainMenu = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnStrtEntering = new System.Windows.Forms.Button();
            this.lblUntenteredVideos = new System.Windows.Forms.Label();
            this.lblCurrDirText = new System.Windows.Forms.Label();
            this.lblCurrDir = new System.Windows.Forms.Label();
            this.btnSelectExcelFile = new System.Windows.Forms.Button();
            this.lblSelExcelFile = new System.Windows.Forms.Label();
            this.lblTextForSelExcelFile = new System.Windows.Forms.Label();
            this.listBoxUnenteredVideos = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSelOverallDirectory
            // 
            this.btnSelOverallDirectory.Location = new System.Drawing.Point(12, 48);
            this.btnSelOverallDirectory.Name = "btnSelOverallDirectory";
            this.btnSelOverallDirectory.Size = new System.Drawing.Size(135, 23);
            this.btnSelOverallDirectory.TabIndex = 0;
            this.btnSelOverallDirectory.Text = "Select Main Directory";
            this.btnSelOverallDirectory.UseVisualStyleBackColor = true;
            this.btnSelOverallDirectory.Click += new System.EventHandler(this.btnSelOverallDirectory_Click);
            // 
            // lblMainMenu
            // 
            this.lblMainMenu.AutoSize = true;
            this.lblMainMenu.Font = new System.Drawing.Font("Calibri", 12F);
            this.lblMainMenu.Location = new System.Drawing.Point(12, 4);
            this.lblMainMenu.Name = "lblMainMenu";
            this.lblMainMenu.Size = new System.Drawing.Size(83, 19);
            this.lblMainMenu.TabIndex = 2;
            this.lblMainMenu.Text = "Main Menu";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listBoxUnenteredVideos);
            this.panel1.Controls.Add(this.btnStrtEntering);
            this.panel1.Controls.Add(this.lblUntenteredVideos);
            this.panel1.Location = new System.Drawing.Point(12, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(858, 433);
            this.panel1.TabIndex = 3;
            // 
            // btnStrtEntering
            // 
            this.btnStrtEntering.Location = new System.Drawing.Point(658, 407);
            this.btnStrtEntering.Name = "btnStrtEntering";
            this.btnStrtEntering.Size = new System.Drawing.Size(187, 23);
            this.btnStrtEntering.TabIndex = 3;
            this.btnStrtEntering.Text = "Start entering from the top of the list";
            this.btnStrtEntering.UseVisualStyleBackColor = true;
            this.btnStrtEntering.Click += new System.EventHandler(this.btnStrtEntering_Click);
            // 
            // lblUntenteredVideos
            // 
            this.lblUntenteredVideos.AutoSize = true;
            this.lblUntenteredVideos.Location = new System.Drawing.Point(13, 30);
            this.lblUntenteredVideos.Name = "lblUntenteredVideos";
            this.lblUntenteredVideos.Size = new System.Drawing.Size(92, 13);
            this.lblUntenteredVideos.TabIndex = 1;
            this.lblUntenteredVideos.Text = "Unentered Videos";
            // 
            // lblCurrDirText
            // 
            this.lblCurrDirText.AutoSize = true;
            this.lblCurrDirText.Location = new System.Drawing.Point(153, 53);
            this.lblCurrDirText.Name = "lblCurrDirText";
            this.lblCurrDirText.Size = new System.Drawing.Size(97, 13);
            this.lblCurrDirText.TabIndex = 4;
            this.lblCurrDirText.Text = "Selected Directory:";
            // 
            // lblCurrDir
            // 
            this.lblCurrDir.AutoSize = true;
            this.lblCurrDir.Location = new System.Drawing.Point(246, 54);
            this.lblCurrDir.Name = "lblCurrDir";
            this.lblCurrDir.Size = new System.Drawing.Size(32, 13);
            this.lblCurrDir.TabIndex = 5;
            this.lblCurrDir.Text = "None";
            // 
            // btnSelectExcelFile
            // 
            this.btnSelectExcelFile.Location = new System.Drawing.Point(12, 24);
            this.btnSelectExcelFile.Name = "btnSelectExcelFile";
            this.btnSelectExcelFile.Size = new System.Drawing.Size(135, 23);
            this.btnSelectExcelFile.TabIndex = 6;
            this.btnSelectExcelFile.Text = "Select Excel File to Edit";
            this.btnSelectExcelFile.UseVisualStyleBackColor = true;
            this.btnSelectExcelFile.Click += new System.EventHandler(this.btnSelectExcelFile_Click);
            // 
            // lblSelExcelFile
            // 
            this.lblSelExcelFile.AutoSize = true;
            this.lblSelExcelFile.Location = new System.Drawing.Point(246, 29);
            this.lblSelExcelFile.Name = "lblSelExcelFile";
            this.lblSelExcelFile.Size = new System.Drawing.Size(32, 13);
            this.lblSelExcelFile.TabIndex = 8;
            this.lblSelExcelFile.Text = "None";
            // 
            // lblTextForSelExcelFile
            // 
            this.lblTextForSelExcelFile.AutoSize = true;
            this.lblTextForSelExcelFile.Location = new System.Drawing.Point(153, 29);
            this.lblTextForSelExcelFile.Name = "lblTextForSelExcelFile";
            this.lblTextForSelExcelFile.Size = new System.Drawing.Size(96, 13);
            this.lblTextForSelExcelFile.TabIndex = 7;
            this.lblTextForSelExcelFile.Text = "Selected Excel File:";
            // 
            // listBoxUnenteredVideos
            // 
            this.listBoxUnenteredVideos.FormattingEnabled = true;
            this.listBoxUnenteredVideos.Location = new System.Drawing.Point(16, 46);
            this.listBoxUnenteredVideos.Name = "listBoxUnenteredVideos";
            this.listBoxUnenteredVideos.Size = new System.Drawing.Size(829, 355);
            this.listBoxUnenteredVideos.TabIndex = 4;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(882, 522);
            this.Controls.Add(this.lblSelExcelFile);
            this.Controls.Add(this.lblTextForSelExcelFile);
            this.Controls.Add(this.btnSelectExcelFile);
            this.Controls.Add(this.lblCurrDir);
            this.Controls.Add(this.lblCurrDirText);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblMainMenu);
            this.Controls.Add(this.btnSelOverallDirectory);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMenu";
            this.Text = "Camera Trap Data Entry";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelOverallDirectory;
        private System.Windows.Forms.Label lblMainMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUntenteredVideos;
        private System.Windows.Forms.Label lblCurrDirText;
        private System.Windows.Forms.Label lblCurrDir;
        private System.Windows.Forms.Button btnStrtEntering;
        private System.Windows.Forms.Button btnSelectExcelFile;
        private System.Windows.Forms.Label lblSelExcelFile;
        private System.Windows.Forms.Label lblTextForSelExcelFile;
        private System.Windows.Forms.ListBox listBoxUnenteredVideos;
    }
}

