﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraTrapAuto
{

    class ExcelEntry
    {
        string userInitials;
        string siteName;
        string siteNumber;
        string siteID;
        string revisitDate;
        string generatedFileName;
        string dateOfVideo; //dd:mm:yy
        string timeVideoStart; //24 hr
        string durationVideo; // hr:min:sec
        int humanCount;

        bool researcherCamHandleStart;
        bool researcherCamHandleMid;
        bool researcherCamHandleEnd;
        int maxHumanDrifting;
        int maxDriftboats; // amount of drift boats
        int maxHumansInNONDriftBoat; //human count
        int maxNonDriftboats; //types of other boats
        int maxNoOfHumansFishingFromShore;
        int maxNoOfHumansPlatform;
        int maxNoOtherHumans;
        string humanComments;

        bool nonBearAnimal;
        string nonBearSpecies;

        int bearCount;
        string bearSpecies; //Grizzly (G), black bear (B), N/A for no bear
        
        //sexes M and F, U for unknown, N/A for no bear detected
        //do not sex cubs - only adults and subadults
        //only indicate unknown if there is no way to sex the bear, if there is
        //but you cannot do it, add Y under verification required
        string sex1;
        string sex2;
        string sex3;
        string sex4;

        //ADULT, SUBADULT, U
        string age1;
        string age2;
        string age3;
        string age4;

        string familyGroup; // Y for sow with yearlings, etc., N, or N/A
        string coyCount; // cub of the year count, N/A if no bear detected
        string yearlingCount; //number of yearlings, N/A if no bear detected

        string bearFishInteraction; //Y,N,N/A
        string snagAvoid; //Y,N,N/A
        string snagAvoidComments; //if yes, describe behaviour
        string camAwareness; //Y, N, N/A
        string camAwarenessComments; //describe cam awareness

        string generalComments; //any useful comments such as date errors, etc.
        string verificationRequired; //write column number, e.g. AGE1 for verification, if required


    }
}
