﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace CameraTrapAuto
{
    class RevisitDir
    {
        List<Videos> videos;

        string year;

        string directory;

        int currVideo;
        public RevisitDir(string dir)
        {
            currVideo = 0;
            directory = dir;
            videos = new List<Videos>();
            getVideos();
        }

        void getYear()
        {
            int indOfYear = directory.IndexOf("Revisit Date - ") + "Revisit Date - ".Length;
            year = directory.Substring(indOfYear, 4);
        }

        private void getVideos()
        {

            DirectoryInfo dir = new DirectoryInfo(directory);
            FileInfo[] files = dir.GetFiles();

            var filtered = files.Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden));


            foreach (string currDir in Directory.GetFiles(directory, "*.avi"))
            {
                //get last slah which is the file
                int indOfSlash = currDir.LastIndexOf("\\");
                string subStr = currDir.Substring(indOfSlash, currDir.Length - indOfSlash);
                //don't include if hidden
                if (!subStr.Contains("._"))
                {
                    videos.Add(new Videos(currDir));
                }
            }
        }

        //try and get next video, returns false if we are at the end of the site
        public Videos nextVideo()
        {
            //if the next video is passed the count, return 0
            if(currVideo + 1 > videos.Count)
            {
                return new Videos("NO VIDEO");
            }
            else
            {
                currVideo++;
            }

            if (videos.Count == 0)
            {
                return new Videos("NO VIDEO");
            }

            return videos[currVideo];

        }

        //method to expose the current video
        public Videos GetCurrentVideo()
        {
            if(videos.Count == 0)
            {
                return new Videos("NO VIDEO");
            }
            return videos[currVideo];
        }

        public Videos getFirstVideo()
        {
            if (videos.Count == 0)
            {
                return new Videos("NO VIDEO");
            }
            return videos[0];
        }
    }
}
