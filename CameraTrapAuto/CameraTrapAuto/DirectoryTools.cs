﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
namespace CameraTrapAuto
{
    static class DirectoryTools
    {
        public const string enteredStr = "\\Entered";
        public const string unenteredStr = "\\Unentered";

        public static List<string> getDirectoriesWithString(string dir, string str)
        {
            List<string> directories = new List<string>();
            try
            {
                directories = Directory.GetDirectories(dir, str).ToList();
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to get directory: " + dir + " with search string: " + str + ". Exception: " + e.ToString());
            }

            
            return directories;
        }

        public static List<string> getDirectories(string directory)
        {
            List<string> directories = new List<string>();

            try
            {
                // dirs = Directory.GetFiles(directory, "*.avi");
                directories = Directory.GetDirectories(directory).ToList();
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to get directory: " + directory + ". Exception: " + e.ToString());
            }
            return directories;
        }

    }
}
