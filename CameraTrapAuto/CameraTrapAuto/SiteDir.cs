﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraTrapAuto
{
    public class SiteDir
    {
        public string name;
        public string ID;
        public string directory;
        List<RevisitDir> revisitDirs;
        int currRevDir;

        public SiteDir(string dir)
        {
            currRevDir = 0;
            directory = dir;
            revisitDirs = new List<RevisitDir>();

            //get revisit directories
            foreach (string currDir in DirectoryTools.getDirectories(directory))
            {
                var newRevDir = new RevisitDir(currDir);
                //if there are no videos
                if(newRevDir.getFirstVideo().dir != "NO VIDEO")
                    revisitDirs.Add(newRevDir);
            }
        }

        void getSiteNoAndName()
        {
            int indOfNoStart = directory.LastIndexOf("\\") + 1;
            int indOfEndOfNo = directory.IndexOf(" ", indOfNoStart);
            ID = directory.Substring(indOfNoStart, indOfEndOfNo - indOfNoStart);

            indOfEndOfNo += 1;
            //string siteName = directory.Substring(indOfEndOfNo + 1, (directory.Length) - indOfEndOfNo);
            name = directory.Substring(indOfEndOfNo, directory.Length  - indOfEndOfNo);


            
        }

        //try and get next video, returns false if we are at the end of the revisit
        public Videos nextVideoChkRevisit()
        {
            Videos currVid = revisitDirs[currRevDir].nextVideo();


            //if the video comes back false, then try and move to the next revisit site
            if(currVid.dir =="NO VIDEO")
            {
                //if it is false, then we need to move to the next revisit directory
                if (currRevDir + 1 > revisitDirs.Count)
                {
                    //if the revisit directory isn't there, then return false
                    
                    return new Videos("NO VIDEO");
                }
                else
                {
                    currRevDir++;
                    currVid = revisitDirs[currRevDir].getFirstVideo();
                }
            }
            return currVid;

        }

        public Videos getCurrVideo()
        {
            return revisitDirs[currRevDir].GetCurrentVideo();
        }

    }
}
